<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../qml/AboutDialog.qml" line="31"/>
        <source>About Qrop</source>
        <translation>À propos de Qrop</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="71"/>
        <source>A cross-platform tool for crop planning and recordkeeping. Made by farmers, for farmers with the help of the French coop &lt;a href=&apos;https://latelierpaysan.org&apos;&gt;L&apos;Atelier paysan&lt;/a&gt;.</source>
        <translation>Un outil multiplatorme de planification et de suivi des cultures en maraîchage. Conçu par des maraîcher⋅es, pour des maraîcher⋅es, avec l&apos;aide la coopérative française &lt;a href=&apos;https://latelierpaysan.org&apos;&gt;L&apos;Atelier paysan&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="96"/>
        <source>This program comes with ABSOLUTELY NO WARRANTY, for more details, visit &lt;a href=&apos;https://www.gnu.org/licenses/gpl-3.0.html&apos;&gt;GNU General Public License version 3&lt;/a&gt;.</source>
        <translation>Ce programme est fourni SANS AUCUNE GARANTIE. Pour plus de détails, visitez &lt;a href=&apos;https://www.gnu.org/licenses/gpl-3.0.html&apos;&gt;GNU General Public License version 3&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>AddCropDialog</name>
    <message>
        <location filename="../qml/AddCropDialog.qml" line="40"/>
        <source>Add New Crop</source>
        <translation>Ajouter une nouvelle espèce</translation>
    </message>
    <message>
        <location filename="../qml/AddCropDialog.qml" line="67"/>
        <source>Crop</source>
        <translation>Espèce</translation>
    </message>
    <message>
        <location filename="../qml/AddCropDialog.qml" line="77"/>
        <source>Family</source>
        <translation>Famille</translation>
    </message>
    <message>
        <location filename="../qml/AddCropDialog.qml" line="96"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
</context>
<context>
    <name>AddDialogButtonBox</name>
    <message>
        <location filename="../qml/AddDialogButtonBox.qml" line="39"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../qml/AddDialogButtonBox.qml" line="48"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>AddEditDialogFooter</name>
    <message>
        <location filename="../qml/AddEditDialogFooter.qml" line="31"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../qml/AddEditDialogFooter.qml" line="44"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../qml/AddEditDialogFooter.qml" line="44"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
</context>
<context>
    <name>AddFamilyDialog</name>
    <message>
        <location filename="../qml/AddFamilyDialog.qml" line="15"/>
        <source>Add New Family</source>
        <translation>Ajouter une famille</translation>
    </message>
    <message>
        <location filename="../qml/AddFamilyDialog.qml" line="39"/>
        <source>Family</source>
        <translation>Famille</translation>
    </message>
    <message>
        <location filename="../qml/AddFamilyDialog.qml" line="52"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
</context>
<context>
    <name>AddKeywordDialog</name>
    <message>
        <location filename="../qml/AddKeywordDialog.qml" line="16"/>
        <source>Add New Family</source>
        <translation>Ajouter une famille</translation>
    </message>
    <message>
        <location filename="../qml/AddKeywordDialog.qml" line="33"/>
        <source>Keyword</source>
        <translation>Mot-clef</translation>
    </message>
</context>
<context>
    <name>AddUnitDialog</name>
    <message>
        <location filename="../qml/AddUnitDialog.qml" line="35"/>
        <source>Add Unit</source>
        <translation>Ajouter une unité</translation>
    </message>
    <message>
        <location filename="../qml/AddUnitDialog.qml" line="79"/>
        <source>Full name</source>
        <translation>Libellé complet</translation>
    </message>
    <message>
        <location filename="../qml/AddUnitDialog.qml" line="68"/>
        <source>Abbreviation</source>
        <translation>Abréviation</translation>
    </message>
</context>
<context>
    <name>AddVarietyDialog</name>
    <message>
        <location filename="../qml/AddVarietyDialog.qml" line="36"/>
        <source>Add New Variety</source>
        <translation>Ajouter une variété</translation>
    </message>
    <message>
        <location filename="../qml/AddVarietyDialog.qml" line="69"/>
        <source>Variety</source>
        <translation>Variété</translation>
    </message>
    <message>
        <location filename="../qml/AddVarietyDialog.qml" line="81"/>
        <source>Seed Company</source>
        <translation>Fournisseur</translation>
    </message>
</context>
<context>
    <name>CalendarPage</name>
    <message>
        <source>Task</source>
        <translation type="vanished">Tâche</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="41"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="39"/>
        <source>Plantings</source>
        <translation>Séries</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="40"/>
        <source>Locations</source>
        <translation>Emplacements</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="42"/>
        <source>Due Date</source>
        <translation>Date prévue</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="56"/>
        <source>Task calendar</source>
        <translation>Calendrier des tâches</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="153"/>
        <source>PDF (*.pdf)</source>
        <translation>PDF (*.pdf)</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/CalendarPage.qml" line="243"/>
        <source>%L1 task(s) selected</source>
        <translation>
            <numerusform>%L1 tâche sélectionnée</numerusform>
            <numerusform>%L1 tâches sélectionnées</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="274"/>
        <source>Search Tasks</source>
        <translation>Recherche des tâches</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="287"/>
        <source>Done</source>
        <translation>Fait</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="293"/>
        <source>Due</source>
        <translation>À faire</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="298"/>
        <source>Overdue</source>
        <translation>En retard</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="319"/>
        <location filename="../qml/CalendarPage.qml" line="325"/>
        <source>Print the task calendar</source>
        <translation>Imprimer le calendrier des tâches</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="336"/>
        <source>Current week</source>
        <translation>Semaine courante</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="342"/>
        <source>Current month</source>
        <translation>Mois courant</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="347"/>
        <source>Current year</source>
        <translation>Année courante</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="355"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="364"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="394"/>
        <source>No tasks done, due or overdue for week %1</source>
        <translation>Pas de tâches effectuées, à faire ou en retard pour la semaine %1</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="396"/>
        <source>No tasks done or due for week %1</source>
        <translation>Pas de tâches effectuées ou à faire pour la semaine %1</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="398"/>
        <source>No tasks done or overdue for week %1</source>
        <translation>Pas de tâches effectuées ou en retard pour la semaine %1</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="400"/>
        <location filename="../qml/CalendarPage.qml" line="402"/>
        <source>No tasks due or overdue for week %1</source>
        <translation>Pas de tâches à faire ou en retard pour la semaine %1</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="404"/>
        <source>No tasks done week %1</source>
        <translation>Pas de tâches effectuée la semaine %1</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="406"/>
        <source>No task due for week %1</source>
        <translation>Pas de tâches à faire pour la semaine %1</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="408"/>
        <source>No tasks overdue for week %1</source>
        <translation>Pas de tâches en retard pour la semaine %1</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="410"/>
        <source>No tasks to show</source>
        <translation>Aucune tâche à afficher</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="422"/>
        <source>Check at least one type to see them</source>
        <translation>Sélectionnez au moins un type de tâche pour en voir !</translation>
    </message>
    <message>
        <source>No tasks to show. Check at least one type to see them!</source>
        <translation type="obsolete">Aucune tâche à afficher. Sélectionnez au moins un type de tâche pour en voir !</translation>
    </message>
    <message>
        <source>No tasks for this week</source>
        <translation type="vanished">Pas de tâches cette semaine</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="430"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/CalendarPage.qml" line="576"/>
        <source>%L1 bed @ </source>
        <translation>
            <numerusform>%L1 planche @ </numerusform>
            <numerusform>%L1 planches @ </numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="578"/>
        <source>%L1 bed m @ %L2 rows X %L3 cm</source>
        <translation>%L1 m planche @ %L2 rangs X %L3 cm</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="584"/>
        <source>%L1 trays of %L2 @ %L3 seeds</source>
        <translation>%L1 plaques de %L2 @ %L3 graines</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="586"/>
        <source>%L1 trays of %L2</source>
        <translation>%L1 plaques de %L2</translation>
    </message>
    <message numerus="yes">
        <source>%L1 bed: </source>
        <translation type="vanished">
            <numerusform>%L1 pl. : </numerusform>
            <numerusform>%L1 pl. : </numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="576"/>
        <source>%L1 rows X %L2 cm</source>
        <translation>%L1 rangs X %L2 cm</translation>
    </message>
    <message>
        <source>%L1 bed m, %L2 rows X %L3 cm</source>
        <translation type="vanished">%L1 m pl., %L2 rangs X %L3 cm</translation>
    </message>
    <message>
        <source>%L1 x %L2, %L3 seeds per cell</source>
        <translation type="vanished">%L1 x %L2, %L3 graines trou</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="663"/>
        <source>Move to previous week</source>
        <translation>Reporter à la semaine précédente</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="678"/>
        <source>Move to next week</source>
        <translation>Reporter à la semaine suivante</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="693"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="720"/>
        <source>Click to complete task. Hold to select date.</source>
        <translation>Cliquer pour effectuer la tâche. Clic long pour sélectionner la date.</translation>
    </message>
    <message>
        <source>%L1 x %L2, %3 seeds per cell</source>
        <translation type="vanished">%L1 x %L2, %3 graines par trou</translation>
    </message>
    <message>
        <source>%L1 x %L2</source>
        <translation type="vanished">%L1 x %L2</translation>
    </message>
    <message numerus="yes">
        <source>%L1 x %L2, %3 seed(s) per cell</source>
        <translation type="vanished">
            <numerusform>%L1 x %L2</numerusform>
            <numerusform>%L1 x %L2, %3 graines par trou</numerusform>
        </translation>
    </message>
    <message>
        <source>Cannot remove a sow/plant task. Switch to crop plan to remove the related planting.</source>
        <translation type="vanished">Impossible de supprimer une tâche de semis/plantation. Utiliser le plan de culture pour supprimer la série.</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="755"/>
        <source>Hide plantings and locations details</source>
        <translation>Cacher le détail des séries et emplacements.</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="756"/>
        <source>Show plantings and locations details</source>
        <translation>Afficher le détail des séries et emplacements.</translation>
    </message>
    <message>
        <source>%L1 trays of  %L2</source>
        <translation type="vanished">%L1 plaques de %L2</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="589"/>
        <source>%1%2%3</source>
        <translation>%1%2%3</translation>
    </message>
    <message>
        <source> with </source>
        <translation type="vanished"> avec </translation>
    </message>
    <message>
        <source>%1 with %2</source>
        <translation type="vanished">%1 avec %2</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Rechercher</translation>
    </message>
    <message>
        <location filename="../qml/CalendarPage.qml" line="256"/>
        <source>Add task</source>
        <translation>Ajouter une tâche</translation>
    </message>
</context>
<context>
    <name>ConflictAlertButton</name>
    <message>
        <location filename="../qml/ConflictAlertButton.qml" line="154"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../qml/ConflictAlertButton.qml" line="162"/>
        <source>Unassign</source>
        <translation>Enlever</translation>
    </message>
    <message>
        <location filename="../qml/ConflictAlertButton.qml" line="172"/>
        <source>Split</source>
        <translation>Diviser</translation>
    </message>
</context>
<context>
    <name>DatePicker</name>
    <message>
        <source>W</source>
        <translation type="vanished">S</translation>
    </message>
</context>
<context>
    <name>HarvestDialog</name>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="130"/>
        <source>Add Harvest</source>
        <translation>Ajouter une récolte</translation>
    </message>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="130"/>
        <source>Edit Harvest</source>
        <translation>Éditer la récolte</translation>
    </message>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="173"/>
        <source>Quantity</source>
        <translation>Quantité</translation>
    </message>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="174"/>
        <source>kg</source>
        <translation>kg</translation>
    </message>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="191"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="204"/>
        <source>Labor Time</source>
        <translation>Temps de travail</translation>
    </message>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="210"/>
        <source>h</source>
        <comment>Abbreviaton for hour</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="250"/>
        <source>Unselect all plantings</source>
        <translation>Désélectionner toutes les séries</translation>
    </message>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="251"/>
        <source>Select all plantings</source>
        <translation>Sélectionner toutes les séries</translation>
    </message>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="262"/>
        <source>Current</source>
        <translation>En cours</translation>
    </message>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="265"/>
        <source>Show only currently harvested plantings for date</source>
        <translation>Afficher uniquement les séries en cours de récolte pour la date indiquée</translation>
    </message>
    <message>
        <location filename="../qml/HarvestDialog.qml" line="266"/>
        <source>Show all plantings</source>
        <translation>Afficher toutes les séries</translation>
    </message>
</context>
<context>
    <name>HarvestsPage</name>
    <message>
        <source>I&apos;m a card!</source>
        <translation type="vanished">Je suis une carte !</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="42"/>
        <source>Planting</source>
        <translation>Série</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="43"/>
        <source>Locations</source>
        <translation>Emplacements</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="44"/>
        <source>Quantity</source>
        <translation>Quantité</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="45"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="46"/>
        <source>Time</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="64"/>
        <source>Harvests</source>
        <translation>Récoltes</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="128"/>
        <source>Harvest added</source>
        <translation>Une récolte ajoutée</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="137"/>
        <source>Harvest modified</source>
        <translation>Une récolte modifiée</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="146"/>
        <source>PDF (*.pdf)</source>
        <translation>PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="166"/>
        <source>No harvests for week %1</source>
        <translation>Pas de récoltes pour la semaine %1</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="174"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="272"/>
        <source>Print the harvests list</source>
        <translation>Imprimer la liste des récoltes</translation>
    </message>
    <message numerus="yes">
        <source>%L1 harvest(s) selected</source>
        <translation type="vanished">
            <numerusform>%L1 récolte sélectionnée</numerusform>
            <numerusform>%L1 récoltes sélectionnées</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="212"/>
        <source>Add harvest</source>
        <translation>Ajouter une récolte</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="251"/>
        <source>Search harvests</source>
        <translation>Rechercher des récoltes</translation>
    </message>
    <message>
        <source>Print the task calendar</source>
        <translation type="vanished">Imprimer le calendrier des tâches</translation>
    </message>
    <message>
        <location filename="../qml/HarvestsPage.qml" line="445"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>LightPlantingForm</name>
    <message>
        <location filename="../qml/LightPlantingForm.qml" line="102"/>
        <source>Greenhouse start date</source>
        <translation>Semis plant</translation>
    </message>
    <message>
        <location filename="../qml/LightPlantingForm.qml" line="117"/>
        <source>Field sowing</source>
        <translation>Semis</translation>
    </message>
    <message>
        <location filename="../qml/LightPlantingForm.qml" line="117"/>
        <source>Field planting</source>
        <translation>Plantation</translation>
    </message>
    <message>
        <location filename="../qml/LightPlantingForm.qml" line="132"/>
        <source>First harvest</source>
        <translation>Première récolte</translation>
    </message>
    <message>
        <location filename="../qml/LightPlantingForm.qml" line="147"/>
        <source>Last harvest</source>
        <translation>Dernière récolte</translation>
    </message>
</context>
<context>
    <name>LocationDialog</name>
    <message>
        <location filename="../qml/LocationDialog.qml" line="112"/>
        <source>Add Locations</source>
        <translation>Ajouter des emplacements</translation>
    </message>
    <message>
        <location filename="../qml/LocationDialog.qml" line="112"/>
        <source>Edit Locations</source>
        <translation>Éditer les emplacements</translation>
    </message>
    <message>
        <location filename="../qml/LocationDialog.qml" line="130"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qml/LocationDialog.qml" line="141"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../qml/LocationDialog.qml" line="142"/>
        <source>bed m</source>
        <translation>m planche</translation>
    </message>
    <message>
        <location filename="../qml/LocationDialog.qml" line="158"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../qml/LocationDialog.qml" line="159"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../qml/LocationDialog.qml" line="195"/>
        <source>Greenhouse</source>
        <translation>Sous abris</translation>
    </message>
    <message>
        <source>cm</source>
        <translation type="vanished">cm</translation>
    </message>
    <message>
        <location filename="../qml/LocationDialog.qml" line="178"/>
        <source>Quantity</source>
        <translation>Nombre</translation>
    </message>
</context>
<context>
    <name>LocationView</name>
    <message>
        <location filename="../qml/LocationView.qml" line="287"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
</context>
<context>
    <name>LocationsPage</name>
    <message>
        <location filename="../qml/LocationsPage.qml" line="49"/>
        <source>Locations</source>
        <translation>Emplacements</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="163"/>
        <source>PDF (*.pdf)</source>
        <translation>PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="191"/>
        <source>Edit Crop Map</source>
        <translation>Éditer le parcellaire</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="394"/>
        <source>Search Plantings</source>
        <translation>Rechercher des séries</translation>
    </message>
    <message>
        <source>Show unassigned plantings</source>
        <translation type="vanished">Afficher les séries non placées</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="191"/>
        <source>Assign locations</source>
        <translation>Affecter des emplacements</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="204"/>
        <source>Rotation problem</source>
        <translation>Problème de rotation</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="225"/>
        <source>Add sublocations</source>
        <translation>Ajouter des sous-emplacements</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="225"/>
        <source>Add Locations</source>
        <translation>Ajouter des emplacements</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="243"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="295"/>
        <source>Expand and collapse location levels</source>
        <translation>Déplier et replier les emplacement par niveau</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="322"/>
        <source>F</source>
        <comment>Abbreviation for family</comment>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="322"/>
        <source>C</source>
        <comment>Abbreviation for crop</comment>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="325"/>
        <source>Click to show crop color</source>
        <translation>Cliquer pour afficher les couleurs d&apos;espèce</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="325"/>
        <source>Click to show family color</source>
        <translation>Cliquer pour afficher les couleurs de famille</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="334"/>
        <source>GH</source>
        <comment>Abbreviation for &quot;greenhouse&quot;</comment>
        <translatorcomment>Abréviation pour « sous abris »</translatorcomment>
        <translation>SA</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="342"/>
        <source>Duplicate</source>
        <translation>Dupliquer</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="353"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="361"/>
        <source>Remove selected locations?</source>
        <translation>Supprimer les emplacements sélectionnés ?</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="367"/>
        <source>This will remove the selected locations and their sublocations. The whole planting history will be lost!</source>
        <translation>Cela supprimer tous les emplacements sélectionnés, ainsi que tous leurs sous-emplacement. L&apos;historique des séries sera perdu !</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="385"/>
        <source>Only show empty locations</source>
        <translation>Afficher uniquement les emplacements vides</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="422"/>
        <source>Print the task calendar</source>
        <translation>Imprimer le calendrier des tâches</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="468"/>
        <source>No locations yet</source>
        <translation>Pas encore d&apos;emplacements</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="477"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="548"/>
        <source>Hide the plantings pane</source>
        <translation>Cacher le panneau des séries</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="548"/>
        <source>Show the planting pane</source>
        <translation>Afficher le panneau des séries</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="574"/>
        <source>No more &quot;%1&quot; plantings to assign for this season.</source>
        <translation>Plus de séries « %1 » à affecter pour cette saison</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="575"/>
        <source>No more plantings to assign for this season.</source>
        <translation>Plus de séries à affecter pour cette saison</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="587"/>
        <source>Previous</source>
        <translation>Précédente</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="598"/>
        <source>Next</source>
        <translation>Suivante</translation>
    </message>
    <message>
        <location filename="../qml/LocationsPage.qml" line="612"/>
        <source>Clear search Field</source>
        <translation>Effacer la recherche</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nom</translation>
    </message>
</context>
<context>
    <name>MobilePlantingForm</name>
    <message>
        <location filename="../qml/MobilePlantingForm.qml" line="18"/>
        <source>Add plantings</source>
        <translation>Ajouter des séries</translation>
    </message>
</context>
<context>
    <name>MyComboBox</name>
    <message>
        <location filename="../qml/MyComboBox.qml" line="40"/>
        <source>Add Item</source>
        <translation>Ajouter un élément</translation>
    </message>
    <message>
        <location filename="../qml/MyComboBox.qml" line="35"/>
        <source>Bad input</source>
        <translation>Erreur</translation>
    </message>
</context>
<context>
    <name>MyTextField</name>
    <message>
        <source>Bad input</source>
        <translation type="vanished">Erreur</translation>
    </message>
    <message>
        <location filename="../qml/MyTextField.qml" line="33"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
</context>
<context>
    <name>NoteButton</name>
    <message>
        <location filename="../qml/NoteButton.qml" line="13"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
</context>
<context>
    <name>NoteSideSheet</name>
    <message>
        <location filename="../qml/NoteSideSheet.qml" line="46"/>
        <source>Pictures (*.jpg *.JPG *.jpeg *.JPEG *.png *.PNG *.gif *.GIF)</source>
        <translation>Images (*.jpg *.JPG *.jpeg *.JPEG *.png *.PNG *.gif *.GIF)</translation>
    </message>
    <message>
        <location filename="../qml/NoteSideSheet.qml" line="46"/>
        <source>All files (*)</source>
        <translation>Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../qml/NoteSideSheet.qml" line="83"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../qml/NoteSideSheet.qml" line="126"/>
        <source>No notes for this planting yet</source>
        <translation>Pas encore de notes pour cette série</translation>
    </message>
    <message>
        <location filename="../qml/NoteSideSheet.qml" line="235"/>
        <source>Enter note</source>
        <translation>Entrer une note</translation>
    </message>
    <message>
        <location filename="../qml/NoteSideSheet.qml" line="304"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
</context>
<context>
    <name>NotesEditPage</name>
    <message>
        <location filename="../qml/NotesEditPage.qml" line="26"/>
        <source>New Note</source>
        <translation>Nouvelle note</translation>
    </message>
    <message>
        <location filename="../qml/NotesEditPage.qml" line="54"/>
        <source>Photos</source>
        <translation>Photos</translation>
    </message>
</context>
<context>
    <name>NotesPage</name>
    <message>
        <source>Planting</source>
        <translation type="vanished">Série</translation>
    </message>
    <message>
        <location filename="../qml/NotesPage.qml" line="45"/>
        <source>Locations</source>
        <translation>Emplacements</translation>
    </message>
    <message>
        <source>Quantity</source>
        <translation type="vanished">Quantités</translation>
    </message>
    <message>
        <location filename="../qml/NotesPage.qml" line="41"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">Durée</translation>
    </message>
    <message>
        <location filename="../qml/NotesPage.qml" line="43"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../qml/NotesPage.qml" line="44"/>
        <source>Plantings</source>
        <translation>Séries</translation>
    </message>
    <message>
        <location filename="../qml/NotesPage.qml" line="63"/>
        <source>Harvests</source>
        <translation>Récoltes</translation>
    </message>
    <message>
        <location filename="../qml/NotesPage.qml" line="128"/>
        <source>Harvest added</source>
        <translation>Une récolte ajoutée</translation>
    </message>
    <message>
        <location filename="../qml/NotesPage.qml" line="137"/>
        <source>Harvest modified</source>
        <translation>Une récolte modifiée</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/NotesPage.qml" line="162"/>
        <source>%L1 harvest(s) selected</source>
        <translation>
            <numerusform>%L1 récolte sélectionnée</numerusform>
            <numerusform>%L1 récoltes sélectionnées</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/NotesPage.qml" line="175"/>
        <source>Add note</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/NotesPage.qml" line="208"/>
        <source>Search harvests</source>
        <translation>Rechercher des récoltes</translation>
    </message>
    <message>
        <location filename="../qml/NotesPage.qml" line="400"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>PlantingDialog</name>
    <message>
        <location filename="../qml/PlantingDialog.qml" line="82"/>
        <source>Add planting(s)</source>
        <translation>Ajouter des séries</translation>
    </message>
    <message>
        <location filename="../qml/PlantingDialog.qml" line="82"/>
        <source>Edit planting(s)</source>
        <translation>Éditer des séries</translation>
    </message>
    <message>
        <location filename="../qml/PlantingDialog.qml" line="102"/>
        <source>You have to choose at least a variety to add a planting.</source>
        <translation>Veuillez choisir au moins la variété pour ajouter une série.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Annuler</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">Ajouter</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Éditer</translation>
    </message>
</context>
<context>
    <name>PlantingForm</name>
    <message>
        <source>Crop</source>
        <translation type="vanished">Espèce</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="598"/>
        <source>Variety</source>
        <translation>Variété</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="711"/>
        <source>Single planting</source>
        <translation>Série unique</translation>
    </message>
    <message>
        <source>Planting Type</source>
        <translation type="vanished">Type de série</translation>
    </message>
    <message>
        <source>Field Sowing</source>
        <translation type="vanished">Semis plein champs</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="930"/>
        <source>Field planting</source>
        <translation>Plantation</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="952"/>
        <source>First harvest</source>
        <translation>Première récolte</translation>
    </message>
    <message numerus="yes">
        <source>%n flat(s)</source>
        <translation type="vanished">
            <numerusform>%n plaque</numerusform>
            <numerusform>%n plaques</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1163"/>
        <source>Needed</source>
        <translation>Nécessaires</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1149"/>
        <source>Per gram</source>
        <translation>Par gramme</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="592"/>
        <source>Add new variety &quot;%1&quot;</source>
        <translation>Ajouter la variété « %1 »</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="592"/>
        <source>Add new variety</source>
        <translation>Ajouter une variété</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1150"/>
        <source>Enter a quantity!</source>
        <translation>Entrez une quantité</translation>
    </message>
    <message numerus="yes">
        <source>%n g</source>
        <translation type="vanished">
            <numerusform>%n g</numerusform>
            <numerusform>%n g</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1174"/>
        <source>Harvest &amp; revenue rate</source>
        <translation>Rendements et produits</translation>
    </message>
    <message>
        <source>Add Unit</source>
        <translation type="vanished">Ajouter une unité</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1213"/>
        <source>Yield/bed m</source>
        <translation>Rendement/m planche</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1228"/>
        <source>Price/</source>
        <translation>Prix/</translation>
    </message>
    <message>
        <source>Keywords</source>
        <translation type="vanished">Mots-clef</translation>
    </message>
    <message>
        <source>kg</source>
        <translation type="vanished">kg</translation>
    </message>
    <message>
        <source>bunch</source>
        <translation type="vanished">botte</translation>
    </message>
    <message>
        <source>pound</source>
        <translation type="vanished">livre</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="739"/>
        <source>Direct seed</source>
        <translation>Semis direct</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="759"/>
        <source>Transplant, raised</source>
        <translation>Plant, fait</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="773"/>
        <source>Transplant, bought</source>
        <translation>Plant, acheté</translation>
    </message>
    <message>
        <source>Amounts</source>
        <translation type="vanished">Quantités</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="703"/>
        <source>Successions</source>
        <translation>Séries</translation>
    </message>
    <message>
        <source>Time between</source>
        <translation type="vanished">Intervalle</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1183"/>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="715"/>
        <source>Weeks between</source>
        <translation>Semaines d&apos;intervalle</translation>
    </message>
    <message>
        <source>Add Crop</source>
        <translation type="vanished">Ajouter une espèce</translation>
    </message>
    <message>
        <source>Add Variety</source>
        <translation type="vanished">Ajouter une variété</translation>
    </message>
    <message>
        <source>Add New Variety</source>
        <translation type="vanished">Ajouter une variété</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="656"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="656"/>
        <source># of beds</source>
        <translation>Nb. de planches</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="657"/>
        <source>bed m</source>
        <translation>m planche</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PlantingForm.qml" line="657"/>
        <source>bed</source>
        <translation>
            <numerusform>planche</numerusform>
            <numerusform>planches</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="672"/>
        <source>Spacing</source>
        <translation>Espacement</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="673"/>
        <source>cm</source>
        <translation>cm</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="684"/>
        <source>Rows</source>
        <translation>Rangs</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="782"/>
        <source>Durations</source>
        <translation>Durées</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="930"/>
        <source>Field sowing</source>
        <translation>Semis</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="970"/>
        <source>Last harvest</source>
        <translation>Dernière récolte</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1003"/>
        <source>Choose locations</source>
        <translation>Sélectionner des emplacements</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1004"/>
        <source>Locations: %1</source>
        <translation>Emplacements: %1</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1017"/>
        <source>Remaining beds: %L1</source>
        <translation>Planches restantes: %L1</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PlantingForm.qml" line="1018"/>
        <source>Remaining length: %L1 m</source>
        <translation>
            <numerusform>Longueur restante : %L1 m</numerusform>
            <numerusform>Longueur restante : %L1 m</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1024"/>
        <source>Unassign all beds</source>
        <translation>Désélectionner toutes les planches</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1034"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PlantingForm.qml" line="1094"/>
        <source>%L1 flat(s) − %L2 transplants</source>
        <translation>
            <numerusform>%L1 plaque − %L2 plants</numerusform>
            <numerusform>%L1 plaques − %L2 plants</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1115"/>
        <source>Seeds per hole</source>
        <translation>Graines par trou</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1135"/>
        <source>Number of seeds: %L1</source>
        <translation>Nombre de graines : %L1</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PlantingForm.qml" line="1151"/>
        <source>Quantity: %L1 g</source>
        <translation>
            <numerusform>Quantité: %L1 g</numerusform>
            <numerusform>Quantité: %L1 g</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1184"/>
        <source>Add the unit &quot;%1&quot;</source>
        <translation>Ajouter l&apos;unité « %1 »</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1184"/>
        <source>Add a unit</source>
        <translation>Ajouter une unité</translation>
    </message>
    <message numerus="yes">
        <source>%L1 g</source>
        <translation type="vanished">
            <numerusform>%L1 g</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Plants needed: </source>
        <translation type="vanished">Plants nécessaires : </translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="876"/>
        <source>Planting dates</source>
        <translation>Dates</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="876"/>
        <source>(first succession)</source>
        <translation>(première série)</translation>
    </message>
    <message>
        <source>Field Sowing Date</source>
        <translation type="vanished">Semis plein champs</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="809"/>
        <location filename="../qml/PlantingForm.qml" line="843"/>
        <source>Days to maturity</source>
        <translation>Durée de croissance</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="911"/>
        <source>Greenhouse start date</source>
        <translation>Semis plant</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="635"/>
        <source>In Greenhouse</source>
        <translation>Sous abris</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="826"/>
        <source>Greenhouse duration</source>
        <translation>Durée pépinière</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="810"/>
        <location filename="../qml/PlantingForm.qml" line="827"/>
        <location filename="../qml/PlantingForm.qml" line="844"/>
        <location filename="../qml/PlantingForm.qml" line="861"/>
        <source>days</source>
        <translation>jours</translation>
    </message>
    <message>
        <source>Field planting date</source>
        <translation type="vanished">Plantation</translation>
    </message>
    <message>
        <source>First harvest date</source>
        <translation type="vanished">Première récolte</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="860"/>
        <source>Harvest window</source>
        <translation>Durée de récolte</translation>
    </message>
    <message>
        <source>Last: </source>
        <translation type="vanished">Dernière : </translation>
    </message>
    <message numerus="yes">
        <source>%L1 flat(s)</source>
        <translation type="vanished">
            <numerusform>%L1 plaque</numerusform>
            <numerusform>%L1 plaques</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1105"/>
        <source>Seeds</source>
        <translation>Semences</translation>
    </message>
    <message>
        <source>Seeds needed</source>
        <translation type="vanished">Semences nécessaires</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1131"/>
        <source>Extra %</source>
        <translation>% supplémentaire</translation>
    </message>
    <message>
        <source>Seeds/g</source>
        <translation type="vanished">Graines/g</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1075"/>
        <source>Greenhouse details</source>
        <translation>Serre</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1082"/>
        <source>Flat type</source>
        <translation>Type de plaque</translation>
    </message>
    <message>
        <source>Seeds per cell</source>
        <translation type="vanished">Graine par trou</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1092"/>
        <source>Estimated loss</source>
        <translation>Perte estimée</translation>
    </message>
    <message>
        <location filename="../qml/PlantingForm.qml" line="1093"/>
        <source>%</source>
        <translation>%</translation>
    </message>
</context>
<context>
    <name>PlantingFormHeader</name>
    <message>
        <location filename="../qml/PlantingFormHeader.qml" line="64"/>
        <source>Bulk edit</source>
        <translation>Édition par lot</translation>
    </message>
    <message>
        <source>Add Crop</source>
        <translation type="vanished">Ajouter une espèce</translation>
    </message>
    <message>
        <location filename="../qml/PlantingFormHeader.qml" line="111"/>
        <source>Add new crop &quot;%1&quot;</source>
        <translation>Ajouter l&apos;espèce « %1 »</translation>
    </message>
    <message>
        <location filename="../qml/PlantingFormHeader.qml" line="111"/>
        <source>Add new crop</source>
        <translation>Ajouter une espèce</translation>
    </message>
    <message>
        <location filename="../qml/PlantingFormHeader.qml" line="154"/>
        <source>Yield</source>
        <translation>Rendement</translation>
    </message>
    <message>
        <location filename="../qml/PlantingFormHeader.qml" line="171"/>
        <source>Revenue</source>
        <translation>Chiffre d&apos;affaire</translation>
    </message>
</context>
<context>
    <name>PlantingLabel</name>
    <message>
        <location filename="../qml/PlantingLabel.qml" line="54"/>
        <source>%1 − %2</source>
        <translation>%1 − %2</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PlantingLabel.qml" line="60"/>
        <source> ⋅ %L1 bed ⋅ %2</source>
        <translation>
            <numerusform> ⋅ %L1 pl. ⋅ %2</numerusform>
            <numerusform> ⋅ %L1 pl. ⋅ %2</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PlantingLabel.qml" line="62"/>
        <source> ⋅ %L1 bed m ⋅ %2</source>
        <translation> ⋅ %L1 mp ⋅ %2</translation>
    </message>
    <message>
        <source>%1 − %2 ⋅ %3 bed m ⋅ %4</source>
        <translation type="vanished">%1 − %2 ⋅ %3 mp ⋅ %4</translation>
    </message>
</context>
<context>
    <name>PlantingsChartPane</name>
    <message>
        <location filename="../qml/PlantingsChartPane.qml" line="36"/>
        <source>Revenue and Space graphs</source>
        <translation>Graphiques d&apos;occupation et de revenu</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsChartPane.qml" line="54"/>
        <source>Estimated field and greenhouse space occupied this year (of X bed m.)</source>
        <translation>Estimation de l&apos;occupation au champs et sous abris pour cette année (sur X m de planche)</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsChartPane.qml" line="141"/>
        <source>Field</source>
        <translation>Plein champs</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsChartPane.qml" line="176"/>
        <source>Greenhouse</source>
        <translation>Sous abris</translation>
    </message>
</context>
<context>
    <name>PlantingsPage</name>
    <message>
        <source>Crop</source>
        <translation type="vanished">Espèce</translation>
    </message>
    <message>
        <source>Variety</source>
        <translation type="vanished">Variété</translation>
    </message>
    <message>
        <source>Seeding Date</source>
        <translation type="vanished">Date semis</translation>
    </message>
    <message>
        <source>Planting Date</source>
        <translation type="vanished">Date plantation</translation>
    </message>
    <message>
        <source>Beg. of harvest</source>
        <translation type="vanished">Déb. récolte</translation>
    </message>
    <message>
        <source>End of harvest</source>
        <translation type="vanished">Fin récolte</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Rechercher</translation>
    </message>
    <message>
        <source>Add planting</source>
        <translation type="vanished">Ajouter une série</translation>
    </message>
    <message>
        <source>Sowing</source>
        <translation type="vanished">Semis</translation>
    </message>
    <message>
        <source>Planting</source>
        <translation type="vanished">Plantation</translation>
    </message>
    <message>
        <source>Begin</source>
        <translation type="vanished">Début</translation>
    </message>
    <message>
        <source>End</source>
        <translation type="vanished">Fin</translation>
    </message>
    <message>
        <source>DTT</source>
        <translation type="vanished">Durée pépinière</translation>
    </message>
    <message>
        <source>DTM</source>
        <translation type="vanished">Durée croissance</translation>
    </message>
    <message>
        <source>Harvest Window</source>
        <translation type="vanished">Récolte</translation>
    </message>
    <message>
        <source>Avg. Yield</source>
        <translation type="vanished">Rendement moyen</translation>
    </message>
    <message>
        <source>Avg. Price</source>
        <translation type="vanished">Prix moyen</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PlantingsPage.qml" line="223"/>
        <source>Added %L1 planting(s)</source>
        <translation>
            <numerusform>%L1 série ajoutée</numerusform>
            <numerusform>%L1 séries ajoutées</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PlantingsPage.qml" line="241"/>
        <source>Modified %L1 planting(s)</source>
        <translation>
            <numerusform>%L1 série modifiée</numerusform>
            <numerusform>%L1 séries modifiées</numerusform>
        </translation>
    </message>
    <message>
        <source>Click on &quot;Add Plantings&quot; to begin planning!</source>
        <translation type="vanished">Cliquez sur « Ajouter des séries » pour commencer à planifier !</translation>
    </message>
    <message>
        <source>Harvest window</source>
        <translation type="vanished">Durée de récolte</translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Rows</source>
        <translation type="vanished">Rangs</translation>
    </message>
    <message>
        <source>Spacing</source>
        <translation type="vanished">Espacement</translation>
    </message>
    <message>
        <source>Revenue and Space graphs</source>
        <translation type="vanished">Graphiques d&apos;occupation et de revenu</translation>
    </message>
    <message>
        <source>Estimated field and greenhouse space occupied this year (of X bed m.)</source>
        <translation type="vanished">Estimation de l&apos;occupation au champs et sous abris pour cette année (sur X m de planche)</translation>
    </message>
    <message>
        <source>Field</source>
        <translation type="vanished">Plein champs</translation>
    </message>
    <message>
        <source>Greenhouse</source>
        <translation type="vanished">Sous abris</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="86"/>
        <source>Plantings</source>
        <translation>Plan de culture</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="259"/>
        <source>Crop plan of %1 duplicated to %2</source>
        <translation>Plan de culture de %1 dupliqué pour %2</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="269"/>
        <source>PDF (*.pdf)</source>
        <translation>PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="299"/>
        <location filename="../qml/PlantingsPage.qml" line="311"/>
        <source>CSV (*.csv)</source>
        <translation>CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="384"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="397"/>
        <source>Duplicate</source>
        <translation>Dupliquer</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="408"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="417"/>
        <source>Duplicate to next year</source>
        <translation>Dupliquer pour l&apos;année suivante</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="474"/>
        <source>Crop plan</source>
        <translation>Plan de culture</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="478"/>
        <source>Export as PDF...</source>
        <translation>Exporter au format PDF...</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="483"/>
        <source>Duplicate crop plan...</source>
        <translation>Dupliquer le plan de culture...</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="488"/>
        <source>Import crop plan...</source>
        <translation>Importer un plan de culture</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="493"/>
        <source>Export crop plan...</source>
        <translation>Exporter le plan de culture</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="560"/>
        <source>Duplicate crop plan</source>
        <translation>Dupliquer le plan de culture</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="604"/>
        <source>From</source>
        <translation>De</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="614"/>
        <source>To</source>
        <translation>Vers</translation>
    </message>
    <message>
        <source>Print the crop plan</source>
        <translation type="vanished">Imprimer le plan de culture</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="500"/>
        <source>Print crop plan</source>
        <translation>Imprimer le plan de culture</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="513"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="517"/>
        <source>Entire plan</source>
        <translation>Plan complet</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="518"/>
        <source>Greenhouse plan</source>
        <translation>Plan de pépinière</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="519"/>
        <source>Field sowing plan</source>
        <translation>Plan de semis direct</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="520"/>
        <source>Transplanting plan</source>
        <translation>Plan de plantation</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="527"/>
        <source>Date range</source>
        <translation>Période</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="530"/>
        <source>Current week</source>
        <translation>Semaine courante</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="531"/>
        <source>Current month</source>
        <translation>Mois en cours</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="532"/>
        <source>Current year</source>
        <translation>Année courante</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="542"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="551"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PlantingsPage.qml" line="446"/>
        <source>planting(s) selected</source>
        <translation>
            <numerusform>%n série sélectionnée</numerusform>
            <numerusform>%Ln séries sélectionnées</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="644"/>
        <source>No plantings for this season</source>
        <translation>Pas de séries pour cette saison</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="652"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <source>No plantings for this season. Click on &quot;Add Plantings&quot; to begin planning!</source>
        <translation type="vanished">Aucune série pour cette saison
Cliquez sur « Ajouter des séries » pour commencer à planifier !</translation>
    </message>
    <message>
        <source>Spring</source>
        <translation type="vanished">Printemps</translation>
    </message>
    <message>
        <source>Summer</source>
        <translation type="vanished">Été</translation>
    </message>
    <message>
        <source>Fall</source>
        <translation type="vanished">Automne</translation>
    </message>
    <message>
        <source>Winter</source>
        <translation type="vanished">Hiver</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="376"/>
        <source>Hide timegraph</source>
        <translation>Cacher le diagramme</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="376"/>
        <source>Show timegraph</source>
        <translation>Afficher le diagrame</translation>
    </message>
    <message>
        <source>Click on &quot;Add Crop&quot; to begin planning!</source>
        <translation type="vanished">Cliquez sur « Ajouter une série » pour commencer à planifier vos cultures !</translation>
    </message>
    <message numerus="yes">
        <source>%n d</source>
        <comment>Abbreviation for day</comment>
        <translation type="vanished">
            <numerusform>%n j</numerusform>
            <numerusform>%n j</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n day(s)</source>
        <translation type="vanished">
            <numerusform>%n jour</numerusform>
            <numerusform>%n jours</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PlantingsPage.qml" line="355"/>
        <source>Add plantings</source>
        <translation>Ajouter des séries</translation>
    </message>
</context>
<context>
    <name>PlantingsView</name>
    <message>
        <location filename="../qml/PlantingsView.qml" line="37"/>
        <source>Crop</source>
        <translation>Espèce</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="43"/>
        <source>Variety</source>
        <translation>Variété</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="49"/>
        <source>Locations</source>
        <translation>Emplacements</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="55"/>
        <source>Sowing</source>
        <translation>Semis</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="61"/>
        <source>Planting</source>
        <translation>Plantation</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="68"/>
        <source>Begin</source>
        <translation>Début</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="75"/>
        <source>End</source>
        <translation>Fin</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="82"/>
        <source>DTT</source>
        <translation>Durée pépinière</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="89"/>
        <source>DTM</source>
        <translation>Durée croissance</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="96"/>
        <source>Harvest Window</source>
        <translation>Récolte</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="103"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="110"/>
        <source>Rows</source>
        <translation>Rangs</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="117"/>
        <source>Spacing</source>
        <translation>Espacement</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="124"/>
        <source>Avg. Yield</source>
        <translation>Rendement moyen</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="131"/>
        <source>Avg. Price</source>
        <translation>Prix moyen</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="138"/>
        <source>Tags</source>
        <translation>Étiquettes</translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="555"/>
        <location filename="../qml/PlantingsView.qml" line="556"/>
        <location filename="../qml/PlantingsView.qml" line="557"/>
        <source>%L1 d</source>
        <comment>Abbreviation for day</comment>
        <translation>%L1 j</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PlantingsView.qml" line="559"/>
        <source>%L1 bed</source>
        <translation>
            <numerusform>%L1 pl.</numerusform>
            <numerusform>%L1 pl.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PlantingsView.qml" line="560"/>
        <source>%L1 m</source>
        <comment>Abbreviation for meter</comment>
        <translation>%L1 m</translation>
    </message>
</context>
<context>
    <name>SearchField</name>
    <message>
        <location filename="../qml/SearchField.qml" line="31"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
</context>
<context>
    <name>SeasonSpinBox</name>
    <message>
        <location filename="../qml/SeasonSpinBox.qml" line="31"/>
        <source>Spring</source>
        <translation>Printemps</translation>
    </message>
    <message>
        <location filename="../qml/SeasonSpinBox.qml" line="32"/>
        <source>Summer</source>
        <translation>Été</translation>
    </message>
    <message>
        <location filename="../qml/SeasonSpinBox.qml" line="33"/>
        <source>Fall</source>
        <translation>Automne</translation>
    </message>
    <message>
        <location filename="../qml/SeasonSpinBox.qml" line="30"/>
        <source>Winter</source>
        <translation>Hiver</translation>
    </message>
    <message>
        <source>Previous year</source>
        <translation type="vanished">Année précédente</translation>
    </message>
    <message>
        <location filename="../qml/SeasonSpinBox.qml" line="150"/>
        <source>Previous season</source>
        <translation>Saison précédente</translation>
    </message>
    <message>
        <location filename="../qml/SeasonSpinBox.qml" line="139"/>
        <source>Next season</source>
        <translation>Saison suivante</translation>
    </message>
    <message>
        <source>Next year</source>
        <translation type="vanished">Année suivante</translation>
    </message>
</context>
<context>
    <name>SeedsPage</name>
    <message>
        <location filename="../qml/SeedsPage.qml" line="42"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="43"/>
        <source>Crop</source>
        <translation>Espèce</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="44"/>
        <source>Variety</source>
        <translation>Variété</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="45"/>
        <source>Seed company</source>
        <translation>Fournisseur</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="46"/>
        <source>Number</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="47"/>
        <source>Quantity</source>
        <translation>Quantité</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="66"/>
        <source>Seed &amp; transplant lists</source>
        <translation>Listes des semences et plants</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="127"/>
        <source>PDF (*.pdf)</source>
        <translation>PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="147"/>
        <source>No seeds to order for %1</source>
        <translation>Pas de semences à commander pour %1</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="158"/>
        <source>No transplants to order for %1</source>
        <translation>Pas de plants à commander pour %1</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="189"/>
        <source>Seeds</source>
        <translation>Semences</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="194"/>
        <source>Transplants</source>
        <translation>Plants</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="200"/>
        <source>Search seeds</source>
        <translation>Recherche des semences</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="201"/>
        <source>Search transplants</source>
        <translation>Rechercher des plants</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="223"/>
        <source>Print the seed order list</source>
        <translation>Imprimer la liste des semences</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="224"/>
        <source>Print the transplant order list</source>
        <translation>Imprimer la liste des plants</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="397"/>
        <source>%L1 kg</source>
        <translation>%L1 kg</translation>
    </message>
    <message>
        <location filename="../qml/SeedsPage.qml" line="398"/>
        <source>%L1 g</source>
        <translation>%L1 g</translation>
    </message>
</context>
<context>
    <name>SettingsCropDelegate</name>
    <message>
        <location filename="../qml/SettingsCropDelegate.qml" line="95"/>
        <source>Remove crop</source>
        <translation>Supprimer l&apos;espèce</translation>
    </message>
    <message>
        <location filename="../qml/SettingsCropDelegate.qml" line="103"/>
        <source>Delete %1?</source>
        <translation>Supprimer %1 ?</translation>
    </message>
    <message>
        <location filename="../qml/SettingsCropDelegate.qml" line="110"/>
        <source>All plantings will be lost.</source>
        <translation>Toutes les séries seront perdues</translation>
    </message>
    <message>
        <location filename="../qml/SettingsCropDelegate.qml" line="131"/>
        <source>Hide varieties</source>
        <translation>Cacher les variétés</translation>
    </message>
    <message>
        <location filename="../qml/SettingsCropDelegate.qml" line="131"/>
        <source>Show varieties</source>
        <translation>Afficher les variétés</translation>
    </message>
    <message>
        <location filename="../qml/SettingsCropDelegate.qml" line="166"/>
        <source>Add variety</source>
        <translation>Ajouter une variété</translation>
    </message>
</context>
<context>
    <name>SettingsFamilyDelegate</name>
    <message numerus="yes">
        <location filename="../qml/SettingsFamilyDelegate.qml" line="95"/>
        <source>%L1 years</source>
        <translation>
            <numerusform>%1 an</numerusform>
            <numerusform>%1 ans</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/SettingsFamilyDelegate.qml" line="98"/>
        <source>Minimum rotation interval for %1</source>
        <translation>Intervalle minimum dans la rotation pour les %1</translation>
    </message>
    <message>
        <location filename="../qml/SettingsFamilyDelegate.qml" line="111"/>
        <source>Remove family</source>
        <translation>Supprimer la famille</translation>
    </message>
    <message>
        <location filename="../qml/SettingsFamilyDelegate.qml" line="121"/>
        <source>Delete %1?</source>
        <translation>Supprimer %1 ?</translation>
    </message>
    <message>
        <location filename="../qml/SettingsFamilyDelegate.qml" line="127"/>
        <source>All crops and plantings will be lost.</source>
        <translation>Toutes les espèces et séries seront perdues</translation>
    </message>
    <message>
        <location filename="../qml/SettingsFamilyDelegate.qml" line="148"/>
        <source>Hide crops</source>
        <translation>Cacher les espèces</translation>
    </message>
    <message>
        <location filename="../qml/SettingsFamilyDelegate.qml" line="148"/>
        <source>Show crop</source>
        <translation>Afficher les espèces</translation>
    </message>
    <message>
        <location filename="../qml/SettingsFamilyDelegate.qml" line="185"/>
        <source>Add crop</source>
        <translation>Ajouter une espèce</translation>
    </message>
</context>
<context>
    <name>SettingsFamilyPane</name>
    <message>
        <location filename="../qml/SettingsFamilyPane.qml" line="56"/>
        <source>Families and crops</source>
        <translation>Familles et espèces</translation>
    </message>
    <message>
        <location filename="../qml/SettingsFamilyPane.qml" line="63"/>
        <source>Add family</source>
        <translation>Ajouter une famille</translation>
    </message>
</context>
<context>
    <name>SettingsImplementDelegate</name>
    <message>
        <location filename="../qml/SettingsImplementDelegate.qml" line="50"/>
        <source>Remove implement</source>
        <translation>Supprimer l&apos;outil</translation>
    </message>
    <message>
        <location filename="../qml/SettingsImplementDelegate.qml" line="61"/>
        <source>Delete %1?</source>
        <translation>Supprimer %1 ?</translation>
    </message>
    <message>
        <location filename="../qml/SettingsImplementDelegate.qml" line="67"/>
        <source>All related tasks will lose their implement.</source>
        <translation>Toutes les tâches concernées perdront leur outil</translation>
    </message>
</context>
<context>
    <name>SettingsKeywordPane</name>
    <message>
        <location filename="../qml/SettingsKeywordPane.qml" line="37"/>
        <source>Keywords</source>
        <translation>Mots-clefs</translation>
    </message>
    <message>
        <location filename="../qml/SettingsKeywordPane.qml" line="44"/>
        <source>Add keyword</source>
        <translation>Ajouter un mot-clef</translation>
    </message>
    <message>
        <location filename="../qml/SettingsKeywordPane.qml" line="54"/>
        <source>Keyword</source>
        <translation>Mot-clef</translation>
    </message>
    <message>
        <location filename="../qml/SettingsKeywordPane.qml" line="55"/>
        <source>Add New Keyword</source>
        <translation>Ajouter un mot-clef</translation>
    </message>
    <message>
        <location filename="../qml/SettingsKeywordPane.qml" line="114"/>
        <source>Remove keyword</source>
        <translation>Supprimer le mot-clef</translation>
    </message>
    <message>
        <location filename="../qml/SettingsKeywordPane.qml" line="124"/>
        <source>Delete %1?</source>
        <translation>Supprimer %1 ?</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/SettingsPage.qml" line="40"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="80"/>
        <source>Recent the application for modifications to take effect</source>
        <translation>Redémarrer l&apos;applications pour que les changements soient pris en compte</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="106"/>
        <source>General Settings</source>
        <translation>Paramètres généraux</translation>
    </message>
    <message>
        <source>Farm name</source>
        <translation type="vanished">Nom de la ferme</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="151"/>
        <source>Date type</source>
        <translation>Type de date</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="162"/>
        <source>Week</source>
        <translation>Semaine</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="162"/>
        <source>Full</source>
        <translation>Complète</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="181"/>
        <source>Show seed company beside variety names</source>
        <translation>Afficher le nom du fournisseur avec la variété</translation>
    </message>
    <message>
        <source>Restart the application for this take effect.</source>
        <translation type="vanished">Redémarrer l&apos;application pour que la modification soit prise en compte</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="202"/>
        <source>Standard bed length</source>
        <translation>Planches standardisées</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="225"/>
        <source>Bed length</source>
        <translation>Longueur de planche</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="246"/>
        <source>Plantings view</source>
        <translation>Plan de culture</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="268"/>
        <source>Compute from durations by default</source>
        <translation>Calculer à partir des durées par défaut</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="289"/>
        <source>Show duration fields</source>
        <translation>Afficher les champs de durée</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="308"/>
        <source>Field map</source>
        <translation>Parcellaire</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="330"/>
        <source>Show complete name of locations</source>
        <translation>Afficher le nom complet des emplacements</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="351"/>
        <source>Allow plantings conflicts on same location</source>
        <translation>Autoriser les conflits de série sur le même emplacement</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="369"/>
        <source>Harvests</source>
        <translation>Récoltes</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="391"/>
        <source>Show all plantings if there is none in harvest window</source>
        <translation>Afficher toutes les séries si aucune n&apos;est dans la fenêtre de récolte</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="407"/>
        <source>Lists</source>
        <translation>Listes</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="432"/>
        <source>Families, crops and varieties</source>
        <translation>Familles, espèces et variétés</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="459"/>
        <source>Keywords</source>
        <translation>Mot-clefs</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="486"/>
        <source>Seed companies</source>
        <translation>Fournisseurs de semences</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="513"/>
        <source>Task types</source>
        <translation>Types de tâche</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="540"/>
        <source>Units</source>
        <translation>Unités</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="566"/>
        <source>Development options</source>
        <translation>Options de développement</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="591"/>
        <source>Reset database and quit</source>
        <translation>Réinitialiser la base et quitter</translation>
    </message>
</context>
<context>
    <name>SettingsSeedCompanyPane</name>
    <message>
        <source>Units</source>
        <translation type="vanished">Unités</translation>
    </message>
    <message>
        <location filename="../qml/SettingsSeedCompanyPane.qml" line="37"/>
        <source>Seeds companies</source>
        <translation>Fournisseurs de semences</translation>
    </message>
    <message>
        <location filename="../qml/SettingsSeedCompanyPane.qml" line="44"/>
        <source>Add seed company</source>
        <translation>Ajouter un fournisseur de semences</translation>
    </message>
    <message>
        <location filename="../qml/SettingsSeedCompanyPane.qml" line="111"/>
        <source>Remove seed company</source>
        <translation>Supprimer le fournisseur de semences</translation>
    </message>
    <message>
        <location filename="../qml/SettingsSeedCompanyPane.qml" line="121"/>
        <source>Delete %1?</source>
        <translation>Supprimer %1 ?</translation>
    </message>
</context>
<context>
    <name>SettingsTaskMethodDelegate</name>
    <message>
        <location filename="../qml/SettingsTaskMethodDelegate.qml" line="54"/>
        <source>Remove method</source>
        <translation>Supprimer la méthode</translation>
    </message>
    <message>
        <location filename="../qml/SettingsTaskMethodDelegate.qml" line="62"/>
        <source>Delete %1?</source>
        <translation>Supprimer %1 ?</translation>
    </message>
    <message>
        <location filename="../qml/SettingsTaskMethodDelegate.qml" line="68"/>
        <source>All related tasks will lose their method.</source>
        <translation>Toutes les tâches concernées perdront leur méthode</translation>
    </message>
    <message>
        <location filename="../qml/SettingsTaskMethodDelegate.qml" line="89"/>
        <source>Hide implements</source>
        <translation>Cacher les outils</translation>
    </message>
    <message>
        <location filename="../qml/SettingsTaskMethodDelegate.qml" line="89"/>
        <source>Show implements</source>
        <translation>Afficher les outils</translation>
    </message>
    <message>
        <location filename="../qml/SettingsTaskMethodDelegate.qml" line="123"/>
        <location filename="../qml/SettingsTaskMethodDelegate.qml" line="128"/>
        <source>Add implement</source>
        <translation>Ajouter un outil</translation>
    </message>
</context>
<context>
    <name>SettingsTaskPane</name>
    <message>
        <location filename="../qml/SettingsTaskPane.qml" line="37"/>
        <source>Task types</source>
        <translation>Types de tâche</translation>
    </message>
    <message>
        <location filename="../qml/SettingsTaskPane.qml" line="44"/>
        <location filename="../qml/SettingsTaskPane.qml" line="54"/>
        <source>Add type</source>
        <translation>Ajouter un type</translation>
    </message>
</context>
<context>
    <name>SettingsTaskTypeDelegate</name>
    <message>
        <location filename="../qml/SettingsTaskTypeDelegate.qml" line="54"/>
        <source>Remove task type</source>
        <translation>Supprimer le type de tâche</translation>
    </message>
    <message>
        <location filename="../qml/SettingsTaskTypeDelegate.qml" line="63"/>
        <source>Delete %1?</source>
        <translation>Supprimer %1 ?</translation>
    </message>
    <message>
        <location filename="../qml/SettingsTaskTypeDelegate.qml" line="69"/>
        <source>All related tasks will be lost.</source>
        <translation>Toutes les tâches concernées seront perdues</translation>
    </message>
    <message>
        <location filename="../qml/SettingsTaskTypeDelegate.qml" line="90"/>
        <source>Hide methods</source>
        <translation>Cacher les méthodes</translation>
    </message>
    <message>
        <location filename="../qml/SettingsTaskTypeDelegate.qml" line="90"/>
        <source>Show methods</source>
        <translation>Afficher les méthodes</translation>
    </message>
    <message>
        <location filename="../qml/SettingsTaskTypeDelegate.qml" line="129"/>
        <location filename="../qml/SettingsTaskTypeDelegate.qml" line="138"/>
        <source>Add method</source>
        <translation>Ajouter une méthode</translation>
    </message>
</context>
<context>
    <name>SettingsUnitPane</name>
    <message>
        <location filename="../qml/SettingsUnitPane.qml" line="37"/>
        <source>Units</source>
        <translation>Unités</translation>
    </message>
    <message>
        <location filename="../qml/SettingsUnitPane.qml" line="44"/>
        <source>Add unit</source>
        <translation>Ajouter une unité</translation>
    </message>
    <message>
        <location filename="../qml/SettingsUnitPane.qml" line="124"/>
        <source>Remove unit</source>
        <translation>Supprimer l&apos;unité</translation>
    </message>
    <message>
        <location filename="../qml/SettingsUnitPane.qml" line="134"/>
        <source>Delete %1?</source>
        <translation>Supprimer %1 ?</translation>
    </message>
</context>
<context>
    <name>SettingsVarietyDelegate</name>
    <message>
        <location filename="../qml/SettingsVarietyDelegate.qml" line="97"/>
        <source>Remove variety</source>
        <translation>Supprimer la variété</translation>
    </message>
    <message>
        <location filename="../qml/SettingsVarietyDelegate.qml" line="108"/>
        <source>Delete %1?</source>
        <translation>Supprimer %1 ?</translation>
    </message>
    <message>
        <location filename="../qml/SettingsVarietyDelegate.qml" line="114"/>
        <source>All plantings will be lost.</source>
        <translation>Toutes les séries concernées seront perdues.</translation>
    </message>
</context>
<context>
    <name>SimpleAddDialog</name>
    <message>
        <location filename="../qml/SimpleAddDialog.qml" line="21"/>
        <source>Add New Item</source>
        <translation>Ajouter un nouvel élément</translation>
    </message>
</context>
<context>
    <name>TaskDialog</name>
    <message>
        <location filename="../qml/TaskDialog.qml" line="69"/>
        <source>Add Task</source>
        <translation>Ajouter une tâche</translation>
    </message>
    <message>
        <location filename="../qml/TaskDialog.qml" line="69"/>
        <source>Edit Task</source>
        <translation>Éditer la tâche</translation>
    </message>
</context>
<context>
    <name>TaskDialogHeader</name>
    <message>
        <location filename="../qml/TaskDialogHeader.qml" line="78"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../qml/TaskDialogHeader.qml" line="82"/>
        <source>Add new type &quot;%1&quot;</source>
        <translation>Ajouter le type « %1 »</translation>
    </message>
    <message>
        <location filename="../qml/TaskDialogHeader.qml" line="82"/>
        <source>Add new type</source>
        <translation>Ajouter un type</translation>
    </message>
    <message>
        <location filename="../qml/TaskDialogHeader.qml" line="97"/>
        <source>Add Type</source>
        <translation>Ajouter un type</translation>
    </message>
    <message>
        <location filename="../qml/TaskDialogHeader.qml" line="112"/>
        <source>Done on %1. Click to undo.</source>
        <translation>Effectuée le %1. Cliquer pour défaire</translation>
    </message>
    <message>
        <location filename="../qml/TaskDialogHeader.qml" line="113"/>
        <source>Click to complete task. Hold to select date.</source>
        <translation>Cliquer pour effectuer la tâche. Clic long pour sélectionner la date.</translation>
    </message>
</context>
<context>
    <name>TaskForm</name>
    <message>
        <location filename="../qml/TaskForm.qml" line="184"/>
        <source>Method</source>
        <translation>Méthode</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Description</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="187"/>
        <source>Add new method &quot;%1&quot;</source>
        <translation>Ajouter la méthode « %1 »</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="187"/>
        <source>Add new method</source>
        <translation>Ajouter une méthode</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="205"/>
        <source>Add Method</source>
        <translation>Ajouter une méthode</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="221"/>
        <source>Implement</source>
        <translation>Outil</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="223"/>
        <source>Add new implement &quot;%1&quot;</source>
        <translation>Ajouter l&apos;outil « %1 »</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="223"/>
        <source>Add new implement</source>
        <translation>Ajouter un outil</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="240"/>
        <source>Add Implement</source>
        <translation>Ajouter un outil</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="267"/>
        <source>Due Date</source>
        <translation>Date prévue</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="278"/>
        <source>days</source>
        <translation>jours</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="279"/>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="288"/>
        <source>Labor Time</source>
        <translation>Temps de travail</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="294"/>
        <source>h</source>
        <comment>Abbreviaton for hour</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="313"/>
        <source>Plantings</source>
        <translation>Séries</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="318"/>
        <source>Locations</source>
        <translation>Emplacements</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="355"/>
        <source>Unelect all plantings</source>
        <translation>Désélectionner toutes les séries</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="356"/>
        <source>Select all plantings</source>
        <translation>Sélectionner toutes les séries</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="368"/>
        <source>Active plantings</source>
        <translation>Séries actives</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="371"/>
        <source>Show only active plantings for due date</source>
        <translation>Afficher uniquement les séries active pour la date prévue</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="372"/>
        <source>Show all plantings</source>
        <translation>Afficher toutes les séries</translation>
    </message>
    <message>
        <location filename="../qml/TaskForm.qml" line="422"/>
        <source>Selected locations: %1</source>
        <translation>Emplacements sélectionnés : %1</translation>
    </message>
    <message>
        <source>Current plantings</source>
        <translation type="vanished">Séries en cours</translation>
    </message>
</context>
<context>
    <name>TaskRowDelegate</name>
    <message>
        <source>Cannot remove a sow/plant task. Switch to crop plan to remove the related planting.</source>
        <translation type="vanished">Impossible de supprimer une tâche de semis/plantation. Utiliser le plan de culture pour supprimer la série.</translation>
    </message>
    <message>
        <source>Hide plantings and locations details</source>
        <translation type="vanished">Cacher le détail des séries et emplacements.</translation>
    </message>
    <message>
        <source>Show plantings and locations details</source>
        <translation type="vanished">Afficher le détail des séries et emplacements.</translation>
    </message>
    <message>
        <source>%L1 trays of  %L2</source>
        <translation type="vanished">%L1 plaques de %L2</translation>
    </message>
    <message>
        <source>%1%2%3</source>
        <translation type="vanished">%1%2%3</translation>
    </message>
</context>
<context>
    <name>Timegraph</name>
    <message>
        <location filename="../qml/Timegraph.qml" line="32"/>
        <source>beds</source>
        <translation>planches</translation>
    </message>
    <message>
        <location filename="../qml/Timegraph.qml" line="32"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../qml/Timegraph.qml" line="60"/>
        <source>%1, %2 (%L3/%L4 %5 assigned)</source>
        <translation>%1, %2 (%L3/%L4 %5 affectés)</translation>
    </message>
    <message>
        <location filename="../qml/Timegraph.qml" line="61"/>
        <source>%1, %2 (%L3/%L4 %5 to assign)</source>
        <translation>%1, %2 (%L3/%L4 %5 à affecter)</translation>
    </message>
</context>
<context>
    <name>WeekSpinBox</name>
    <message>
        <source>Previous year</source>
        <translation type="vanished">Année précédente</translation>
    </message>
    <message>
        <source>Previous week</source>
        <translation type="vanished">Semaine précédente</translation>
    </message>
    <message>
        <source>Next week</source>
        <translation type="vanished">Semaine suivante</translation>
    </message>
    <message>
        <source>Next year</source>
        <translation type="vanished">Année suivante</translation>
    </message>
    <message>
        <location filename="../qml/WeekSpinBox.qml" line="131"/>
        <source>Next season</source>
        <translation>Saison suivante</translation>
    </message>
    <message>
        <location filename="../qml/WeekSpinBox.qml" line="142"/>
        <source>Previous season</source>
        <translation>Saison précédente</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Dashboard</source>
        <translation type="vanished">Tableau de bord</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="32"/>
        <source>Plantings</source>
        <translation>Séries</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="33"/>
        <source>Tasks</source>
        <translation>Tâches</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="34"/>
        <source>Crop Map</source>
        <translation>Plan de culture</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="35"/>
        <source>Harvests</source>
        <translation>Récoltes</translation>
    </message>
    <message>
        <source>A cross-platform tool for crop planning and recordkeeping. Made by farmers, for farmers with the help of the French coop &lt;a href=&apos;https://latelierpaysan.org&apos;&gt;L&apos;Atelier paysan&lt;/a&gt;.</source>
        <translation type="vanished">Un outil multiplatorme de planification et de suivi des cultures en maraîchage. Conçu par des maraîcher⋅es, pour des maraîcher⋅es, avec l&apos;aide la coopérative française &lt;a href=&apos;https://latelierpaysan.org&apos;&gt;L&apos;Atelier paysan&lt;/a&gt;.</translation>
    </message>
    <message>
        <source>This program comes with ABSOLUTELY NO WARRANTY, for more details, visit &lt;a href=&apos;https://www.gnu.org/licenses/gpl-3.0.html&apos;&gt;GNU General Public License version 3&lt;/a&gt;.</source>
        <translation type="vanished">Ce programme est fourni SANS AUCUNE GARANTIE. Pour plus de détails, visitez &lt;a href=&apos;https://www.gnu.org/licenses/gpl-3.0.html&apos;&gt;GNU General Public License version 3&lt;/a&gt;.</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation type="vanished">Notes</translation>
    </message>
    <message>
        <source>Charts</source>
        <translation type="vanished">Graphiques</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="461"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Fichier</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="vanished">&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="36"/>
        <source>Seed list</source>
        <translation>Liste des semences</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">Quitter</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Aide</translation>
    </message>
    <message>
        <source>About...</source>
        <translation type="vanished">À propos...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="270"/>
        <location filename="../qml/main.qml" line="347"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="476"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="527"/>
        <source>Show the note pane</source>
        <translation>Afficher le panneau des notes</translation>
    </message>
    <message>
        <source>A modern, cross-platform tool for planning and recordkeeping. Made by farmers, for farmers.</source>
        <translation type="vanished">Un outil moderne de planification et de suivi des cultures en maraîchage. Conçu par des maraîcher⋅es, pour des maraîcher⋅es.</translation>
    </message>
    <message>
        <source>Edit Crop Map</source>
        <translation type="vanished">Éditer le parcellaire</translation>
    </message>
    <message>
        <source>About Qrop</source>
        <translation type="vanished">À propos de Qrop</translation>
    </message>
</context>
</TS>
