/*
 * Copyright (C) 2018 André Hoarau <ah@ouvaton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2
import QtCharts 2.0

import io.qrop.components 1.0

Rectangle {
    id: control

    property int estimatedYield: 0
    property int estimatedRevenue: 0
    property bool showYieldAndRevenue: true
    property bool showAddItem: true
    property bool bulkEditMode: false
    property string unitText: ""
    property alias cropField: cropField
    property int cropId: cropField.selectedId
    property string mode

//    signal newCropAdded(int newCropId)
    signal cropSelected()

    function refresh() {
        cropModel.refresh();
    }

    function reset() {
        refresh();
        cropField.reset();
    }

    color: Material.color(Material.Grey, Material.Shade200)
    Material.elevation: 2
    radius: 2
    clip: true
    implicitHeight: 60
    width: parent.width

    CropModel {
        id: cropModel
    }

    Label {
        visible: bulkEditMode
        anchors.centerIn: parent
        text: qsTr("Bulk edit")
        font.family: "Roboto Regular"
        font.pixelSize: Units.fontSizeBodyAndButton
    }

    RowLayout {
        id: rowLayout
        visible: !bulkEditMode
        anchors.fill: parent
        spacing: Units.mediumSpacing
        anchors {
            leftMargin: Units.mediumSpacing
            rightMargin: anchors.leftMargin
            topMargin: Units.smallSpacing
            bottomMargin: anchors.topMargin
        }

        Rectangle {
            id: textIcon
            Layout.alignment: Qt.AlignVCenter
            height: 40
            width: height
            radius: 80
            color: {
                if (cropId > 0) {
                    var map = Crop.mapFromId("crop", cropId);
                    return map['color'];
                } else {
                    return Material.color(Material.Grey, Material.Shade400);
                }
            }

            Text {
                anchors.centerIn: parent
                text: cropId > 0 ? cropField.text.slice(0,2) : ""
                color: "white"
                font { family: "Roboto Regular"; pixelSize: 24 }
            }
        }

        ComboTextField {
            id: cropField
            enabled: mode === "add"
            Layout.topMargin: Units.smallSpacing
            textRole: function (model) { return model.crop; }
            idRole: function (model) { return model.crop_id; }
            showAddItem: true
            addItemText: text ? qsTr('Add new crop "%1"').arg(text) : qsTr("Add new crop")

            Layout.fillWidth: true
            model: cropModel

            onAddItemClicked: {
                addCropDialog.open()
                addCropDialog.prefill(text)
            }

            onSelectedIdChanged: if (selectedId > 0) cropSelected()

            AddCropDialog {
                id: addCropDialog

                // When creating a new crop, we have to wait for dialog to close in order
                // to not lose the focus. We use the newId property as a temporary variable
                // to be used in onClosed.
                property int newId: -1

                width: parent.width

                onRejected: {
                    cropField.text = "";
                    newId = -1
                }

                onAccepted: {
                    var id = Crop.add({"crop" : cropName,
                                       "family_id" : familyId,
                                       "color" : color});
                    cropModel.refresh();
                    cropField.text = cropName
                    newId = id
                }

                onClosed: cropField.selectedId = newId
            }
        }

        ColumnLayout {
            visible: showYieldAndRevenue
            Label {
                text: qsTr("Yield")
                Layout.alignment: Qt.AlignRight
                font { family: "Roboto Regular"; pixelSize: Units.fontSizeCaption }
                color: Qt.rgba(0,0,0, 0.50)
            }
            Label {
                id: estimatedYieldLabel
                text: "%L1 %2".arg(estimatedYield).arg(unitText)
                Layout.alignment: Qt.AlignRight
                font { family: "Roboto Regular"; pixelSize: Units.fontSizeBodyAndButton; }
                color: Qt.rgba(0,0,0, 0.87)
            }
        }
        
        ColumnLayout {
            visible: showYieldAndRevenue
            Label {
                text: qsTr("Revenue")
                font { family: "Roboto Regular"; pixelSize: Units.fontSizeCaption }
                color: Qt.rgba(0,0,0, 0.50)
                Layout.alignment: Qt.AlignRight
            }
            Label {
                id: estimatedRevenueLabel
                text: "%L1 €".arg(estimatedRevenue)
                horizontalAlignment: Text.AlignHCenter
                font { family: "Roboto Regular"; pixelSize: Units.fontSizeBodyAndButton }
                color: Qt.rgba(0,0,0, 0.87)
                Layout.alignment: Qt.AlignRight
            }
        }
    }
}
