<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>BuildInfo</name>
    <message>
        <location filename="../buildinfo.cpp" line="11"/>
        <source>Unkown build version</source>
        <translation>Version de build inconnue</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../db.cpp" line="175"/>
        <location filename="../db.cpp" line="186"/>
        <location filename="../db.cpp" line="187"/>
        <location filename="../db.cpp" line="188"/>
        <source>Alliaceae</source>
        <translation>Alliacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="176"/>
        <location filename="../db.cpp" line="189"/>
        <location filename="../db.cpp" line="190"/>
        <location filename="../db.cpp" line="191"/>
        <location filename="../db.cpp" line="192"/>
        <source>Apiaceae</source>
        <translation>Apiacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="177"/>
        <location filename="../db.cpp" line="193"/>
        <location filename="../db.cpp" line="194"/>
        <location filename="../db.cpp" line="195"/>
        <source>Asteraceae</source>
        <translation>Astéracées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="178"/>
        <location filename="../db.cpp" line="196"/>
        <location filename="../db.cpp" line="197"/>
        <location filename="../db.cpp" line="198"/>
        <location filename="../db.cpp" line="199"/>
        <location filename="../db.cpp" line="200"/>
        <location filename="../db.cpp" line="201"/>
        <location filename="../db.cpp" line="202"/>
        <source>Brassicaceae</source>
        <translation>Brassicacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="179"/>
        <location filename="../db.cpp" line="203"/>
        <location filename="../db.cpp" line="204"/>
        <location filename="../db.cpp" line="205"/>
        <source>Chenopodiaceae</source>
        <translation>Chénopodiacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="180"/>
        <location filename="../db.cpp" line="206"/>
        <location filename="../db.cpp" line="207"/>
        <location filename="../db.cpp" line="208"/>
        <location filename="../db.cpp" line="209"/>
        <location filename="../db.cpp" line="210"/>
        <source>Cucurbitaceae</source>
        <translation>Cucurbitacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="181"/>
        <location filename="../db.cpp" line="211"/>
        <location filename="../db.cpp" line="212"/>
        <location filename="../db.cpp" line="213"/>
        <source>Fabaceae</source>
        <translation>Fabacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="182"/>
        <location filename="../db.cpp" line="214"/>
        <location filename="../db.cpp" line="215"/>
        <location filename="../db.cpp" line="216"/>
        <location filename="../db.cpp" line="217"/>
        <source>Solanaceae</source>
        <translation>Solanacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="183"/>
        <location filename="../db.cpp" line="218"/>
        <source>Valerianaceae</source>
        <translation>Valérianacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="186"/>
        <source>Garlic</source>
        <translation>Ail</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="187"/>
        <source>Onion</source>
        <translation>Ognon</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="188"/>
        <source>Leek</source>
        <translation>Poireau</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="189"/>
        <source>Carrot</source>
        <translation>Carotte</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="190"/>
        <source>Celery</source>
        <translation>Céleri</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="191"/>
        <source>Fennel</source>
        <translation>Fenouil</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="192"/>
        <source>Parsnip</source>
        <translation>Panais</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="193"/>
        <source>Chicory</source>
        <translation>Chicorée</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="194"/>
        <source>Belgian endive</source>
        <translation>Endive</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="195"/>
        <source>Lettuce</source>
        <translation>Laitue</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="196"/>
        <source>Cabbage</source>
        <translation>Chou pommé</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="197"/>
        <source>Brussel Sprouts</source>
        <translation>Chou de Bruxelles</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="198"/>
        <source>Kohlrabi</source>
        <translation>Chou-rave</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="199"/>
        <source>Cauliflower</source>
        <translation>Chou-fleur</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="200"/>
        <source>Broccoli</source>
        <translation>Brocoli</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="201"/>
        <source>Turnip</source>
        <translation>Navet</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="202"/>
        <source>Radish</source>
        <translation>Radis rose</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="203"/>
        <source>Beetroot</source>
        <translation>Betterave</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="204"/>
        <source>Chard</source>
        <translation>Blette</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="205"/>
        <source>Spinach</source>
        <translation>Épinard</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="206"/>
        <source>Cucumber</source>
        <translation>Concombre</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="207"/>
        <source>Zucchini</source>
        <translation>Courgette</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="208"/>
        <source>Melon</source>
        <translation>Melon</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="209"/>
        <source>Watermelon</source>
        <translation>Pastèque</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="210"/>
        <source>Winter squash</source>
        <translation>Courge</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="211"/>
        <source>Bean</source>
        <translation>Haricot</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="212"/>
        <source>Fava bean</source>
        <translation>Fève</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="213"/>
        <source>Pea</source>
        <translation>Pois</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="214"/>
        <source>Eggplant</source>
        <translation>Aubergine</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="215"/>
        <source>Pepper</source>
        <translation>Poivron</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="216"/>
        <source>Potatoe</source>
        <translation>Pomme de terre</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="217"/>
        <source>Tomato</source>
        <translation>Tomate</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="218"/>
        <source>Mâche</source>
        <translation>Mâche</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="220"/>
        <source>Cultivation and Tillage</source>
        <translation>Travail du sol</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="221"/>
        <source>Fertilize and Amend</source>
        <translation>Fertilisation et amendement</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="222"/>
        <source>Irrigate</source>
        <translation>Irrigation</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="223"/>
        <source>Maintenance</source>
        <translation>Maintenance</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="224"/>
        <source>Pest and Disease</source>
        <translation>Parasites et maladies</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="225"/>
        <source>Prune</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="226"/>
        <source>Row Cover and Mulch</source>
        <translation>Bâches</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="227"/>
        <source>Stale Bed</source>
        <translation>Faux semis</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="228"/>
        <source>Thin</source>
        <translation>Éclaircissage</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="229"/>
        <source>Trellis</source>
        <translation>Palissage</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="230"/>
        <source>Weed</source>
        <translation>Désherbage</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="232"/>
        <source>kilogram</source>
        <translation>kilogramme</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="232"/>
        <source>kg</source>
        <translation>kg</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="233"/>
        <source>bunch</source>
        <translation>botte</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="233"/>
        <source>bn</source>
        <translation>bte</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="234"/>
        <source>head</source>
        <translation>pièce</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="234"/>
        <source>hd</source>
        <translation>pc</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="253"/>
        <source>Direct sow</source>
        <translation>Semis direct</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="254"/>
        <source>Greenhouse sow</source>
        <translation>Semis en pépinière</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="255"/>
        <source>Transplant</source>
        <translation>Plantation</translation>
    </message>
    <message>
        <source>Transplant sow</source>
        <translation type="vanished">Plantation</translation>
    </message>
</context>
<context>
    <name>MDate</name>
    <message>
        <location filename="../mdate.cpp" line="180"/>
        <source>Winter</source>
        <translation>Hiver</translation>
    </message>
    <message>
        <location filename="../mdate.cpp" line="182"/>
        <source>Spring</source>
        <translation>Printemps</translation>
    </message>
    <message>
        <location filename="../mdate.cpp" line="184"/>
        <source>Summer</source>
        <translation>Été</translation>
    </message>
    <message>
        <location filename="../mdate.cpp" line="186"/>
        <source>Fall</source>
        <translation>Automne</translation>
    </message>
</context>
<context>
    <name>Planting</name>
    <message>
        <location filename="../planting.cpp" line="747"/>
        <source>Unknown company</source>
        <translation>Fournisseur indéfini</translation>
    </message>
    <message>
        <location filename="../planting.cpp" line="753"/>
        <source>Unkown company</source>
        <translation>Fournisseur indéfini</translation>
    </message>
</context>
<context>
    <name>Print</name>
    <message>
        <location filename="../print.cpp" line="72"/>
        <source>General crop plan</source>
        <translation>Plane de culture général</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="86"/>
        <location filename="../print.cpp" line="128"/>
        <location filename="../print.cpp" line="166"/>
        <location filename="../print.cpp" line="205"/>
        <location filename="../print.cpp" line="290"/>
        <location filename="../print.cpp" line="315"/>
        <source>Crop</source>
        <translation>Espèce</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="87"/>
        <location filename="../print.cpp" line="129"/>
        <location filename="../print.cpp" line="167"/>
        <location filename="../print.cpp" line="206"/>
        <location filename="../print.cpp" line="291"/>
        <location filename="../print.cpp" line="316"/>
        <source>Variety</source>
        <translation>Variété</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="88"/>
        <location filename="../print.cpp" line="127"/>
        <location filename="../print.cpp" line="165"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="89"/>
        <location filename="../print.cpp" line="130"/>
        <location filename="../print.cpp" line="204"/>
        <source>TP</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="90"/>
        <location filename="../print.cpp" line="168"/>
        <location filename="../print.cpp" line="207"/>
        <source>FH</source>
        <translation>DR</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="91"/>
        <source>LH</source>
        <translation>FR</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="92"/>
        <location filename="../print.cpp" line="169"/>
        <location filename="../print.cpp" line="208"/>
        <source>Length</source>
        <translation>Lgr.</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="93"/>
        <location filename="../print.cpp" line="171"/>
        <location filename="../print.cpp" line="210"/>
        <source>Rows</source>
        <translation>Rangs</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="94"/>
        <source>Spac.</source>
        <translation>Dist.</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="95"/>
        <location filename="../print.cpp" line="173"/>
        <location filename="../print.cpp" line="213"/>
        <location filename="../print.cpp" line="242"/>
        <location filename="../print.cpp" line="268"/>
        <source>Locations</source>
        <translation>Emplacements</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="114"/>
        <source>Greenhouse crop plan</source>
        <translation>Plan de culture pépinière</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="131"/>
        <location filename="../print.cpp" line="211"/>
        <source># trays</source>
        <translation>Nb. plaques</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="132"/>
        <location filename="../print.cpp" line="212"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="133"/>
        <source>Seeds/hole</source>
        <translation>Graines/trou</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="134"/>
        <source># seeds</source>
        <translation>Nb. graines</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="135"/>
        <source>Seed weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="152"/>
        <source>Field sowing crop plan</source>
        <translation>Plan de culture semis direct</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="170"/>
        <location filename="../print.cpp" line="209"/>
        <source>Spacing</source>
        <translation>Dist.</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="172"/>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="190"/>
        <source>Field transplanting crop plan</source>
        <translation>Plan de culture plantation</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="230"/>
        <source>Task calendar</source>
        <translation>Calendrier des tâches</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="240"/>
        <location filename="../print.cpp" line="266"/>
        <location filename="../print.cpp" line="314"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="241"/>
        <location filename="../print.cpp" line="267"/>
        <source>Planting</source>
        <translation>Série</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="243"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="244"/>
        <source>Tags</source>
        <translation>Étiquettes</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="245"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="257"/>
        <source>Harvests</source>
        <translation>Récoltes</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="269"/>
        <location filename="../print.cpp" line="294"/>
        <source>Quantity</source>
        <translation>Quantité</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="270"/>
        <source>Labor time</source>
        <translation>Temps de travail</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="281"/>
        <source>Seed list</source>
        <translation>Liste des semences</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="292"/>
        <location filename="../print.cpp" line="317"/>
        <source>Company</source>
        <translation>Fournisseur</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="293"/>
        <location filename="../print.cpp" line="318"/>
        <source>Number</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="305"/>
        <source>Transplant list</source>
        <translation>Liste des plants</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="531"/>
        <source> W%1, %2 − %3</source>
        <translation> S%1, %2 − %3</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="596"/>
        <source>%1 beds</source>
        <translation>%1 pl.</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="598"/>
        <source>%1 bed m.</source>
        <translation>%1 m pl.</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="604"/>
        <location filename="../print.cpp" line="615"/>
        <source>%1, %2 rows x %3 cm</source>
        <translation>%1, %2 rangs x %3 cm</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="609"/>
        <source>%1 x %2, %3 seeds per hole</source>
        <translation>%1 x %2, %3 graines par trou</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="611"/>
        <source>%1 x %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../print.cpp" line="833"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
</context>
</TS>
