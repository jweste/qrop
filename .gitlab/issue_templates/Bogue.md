### Résumé

<!--- Résumer de façon concise le bogue que vous avez trouvé. -->

### Étapes à reproduire

<!--- Comment reproduire le bogue ; très important ! -->

### Quel est le comportement actuel du bogue ?

<!--- Que se passe-t-il ? -->

### Quel le comportement correct attendu ?

<!--- Que devrait-il se passer ? -->

### Copies d'écran, informations supplémentaires

<!--- Si nécessaires, copier/coller ici toute sortie console ; merci d'utiliser
les blocs de code (```), car c'est très difficle à lire sinon. -->

### Corrections possibles

<!--- Si vous le pouvez, indiquez la ou les lignes de code qui peuvent être 
responsables du problème. -->

/label ~bug
